//
//  AnimalTableViewController.swift
//  TabBarApplication
//
//  Created by Admin on 17.02.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import UIKit

class AnimalTableViewController: UITableViewController {

    var animals : [Animal]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let fileReader = JsonFileReader(fileName: "animals", fileExtension: "json")
        animals = fileReader.parceJsonFile()
        if animals == nil{
            showAlert()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(animals == nil){
            return 0
        }
        return animals.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "animalCell", for: indexPath) as? AnimalTableViewCell
        guard let animals = animals else{
            return cell!
        }
        
        cell?.animalImage.image = UIImage(named: (animals[indexPath.row].picturePath))
        cell?.animalShortDescription.text = animals[indexPath.row].name
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "animalDetailed", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let animals = animals else{
            return
        }
        
        let rowSelected = (sender as! IndexPath).row
        
        if let detailedVC = segue.destination as? AnimalDetailedViewController {
            detailedVC.animalDescriptionShort = animals[rowSelected].name
            detailedVC.animalDescriptionLong = animals[rowSelected].description
            detailedVC.animalPicture = UIImage(named: animals[rowSelected].picturePath)
        }
        
    }
    
    @IBAction func showAlert(){
        let alertController = UIAlertController(title: "Internal error", message: "Can't display data", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(alertAction)
        
        present(alertController, animated: true, completion: nil)
    }

}
