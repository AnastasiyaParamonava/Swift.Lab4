//
//  MainViewController.swift
//  Consolidation
//
//  Created by Admin on 23.03.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    
    @IBAction func calculatorButtonClick(_ sender: Any) {
        performSegue(withIdentifier: "ShowCalculator", sender: self)
    }

    @IBAction func animalsAndCurrencyButtonClick(_ sender: Any) {
        performSegue(withIdentifier: "ShowAnimalsAndCurrency", sender: self)
    }
    
    @IBAction func weatherButtonClick(_ sender: Any) {
        performSegue(withIdentifier: "ShowWeather", sender: self)
    }
    
}
