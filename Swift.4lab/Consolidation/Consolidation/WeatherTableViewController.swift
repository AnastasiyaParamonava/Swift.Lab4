//
//  WeatherTableViewController.swift
//  WeatherMap
//
//  Created by Admin on 03.03.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import UIKit
import MapKit

class WeatherTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var table: UITableView!

    @IBOutlet weak var innerMap: MKMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        if GlobalVariables.cities == nil{
            let dataReader = CitiesJsonFileReader()
            GlobalVariables.cities = dataReader.parceJsonFile()
    
            if GlobalVariables.cities == nil{
                showAlert()
                return
            }
            let loader = WeatherLoader(forCities: GlobalVariables.cities!, tableToUpdate: table)
            loader.loadWeather()
        }
        
        let span = MKCoordinateSpanMake(0.1, 0.1)
        let location = CLLocationCoordinate2DMake(51.506794, -0.125637)
        let region = MKCoordinateRegionMake(location, span)
        innerMap.setRegion(region, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if GlobalVariables.cities != nil{
            return GlobalVariables.cities!.count
        }
        return 0
    }

    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath) as! WeatherTableViewCell
        let row = indexPath.row
        
        let cities = GlobalVariables.cities!
        cell.cityLabel.text = cities[row].countryCode + ", " + cities[row].city
        cell.pressureLabel.text = "Pressure: " + String(cities[row].pressure)
        cell.descriptionLabel.text = cities[row].description
        cell.temperatureLabel.text = "Temperature: " + String(cities[row].temperatureInCelcius) + "C"
        cell.windSpeedLabel.text = "Wind speed: " + String(cities[row].windSpeed)

        return cell
    }
    

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation){
            performSegue(withIdentifier: "weatherDetailed", sender: indexPath)
        }
        else{
            let row = indexPath.row
            addWeatherPin(city: GlobalVariables.cities[row])
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let cities = GlobalVariables.cities else{
            return
        }
        
        let row = (sender as! IndexPath).row
        
        if let detailedVC = segue.destination as? WeatherDetailedViewController {
            detailedVC.cityWeatherDescription = cities[row]
        }
        
    }
    
    
    private func addWeatherPin(city : CityWeatherDescription){
        let span = MKCoordinateSpanMake(0.1, 0.1)
        let location = CLLocationCoordinate2DMake(city.lat, city.lon)
        let region = MKCoordinateRegionMake(location, span)
        innerMap.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = city.city + ": " + city.description
        annotation.subtitle = "Temperature: " + String(city.temperatureInCelcius) + "C, " + "Pressure: " + String(city.pressure)
        self.innerMap.removeAnnotations(innerMap.annotations)
        self.innerMap.addAnnotation(annotation)
    }

    
    @IBAction func showAlert(){
        let alertController = UIAlertController(title: "Internal error", message: "Can't display data", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(alertAction)
        
        present(alertController, animated: true, completion: nil)
    }


}
