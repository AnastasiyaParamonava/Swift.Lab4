//
//  RateTableViewController.swift
//  TabBarApplication
//
//  Created by Admin on 21.02.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import UIKit

class RateTableViewController: UITableViewController {
    
    var rates : [ExchangeRate]!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let rateLoader = RateLoader(serviceUrl: "http://api.fixer.io/latest")
        
        rates = rateLoader.loadRates()
        if(rates == nil){
            showAlert()
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(rates == nil){
            return 0
        }
        return rates.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "rateCell", for: indexPath) as? RateTableViewCell
        guard let rates = rates else{
            return cell!
        }
        
        cell?.locationLabel.text = rates[indexPath.row].location
        cell?.rateLabel.text = rates[indexPath.row].exchangeRate
        
        return cell!
    }
    
    @IBAction func showAlert(){
        let alertController = UIAlertController(title: "Internal error", message: "Can't load data", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(alertAction)
        
        present(alertController, animated: true, completion: nil)
    }
}
