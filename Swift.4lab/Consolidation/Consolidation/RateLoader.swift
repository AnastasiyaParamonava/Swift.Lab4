//
//  RateLoader.swift
//  TabBarApplication
//
//  Created by Admin on 21.02.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import Foundation
import SwiftyJSON

class RateLoader{
    
    var serviceUrl : String
    
    init(serviceUrl : String) {
        self.serviceUrl = serviceUrl
    }
    
    func loadRates() -> [ExchangeRate]! {
        
        var result: [ExchangeRate]! = []
        let url = URL(string: serviceUrl)
        let semaphore = DispatchSemaphore(value: 0)
        URLSession.shared.dataTask(with: url!){
            data, response, error in
            guard error == nil else{
                print(error!)
                return
            }
            
            guard let content = data else{
                return
            }
            
            let jsonData = JSON(data: content, options: JSONSerialization.ReadingOptions.mutableContainers, error: nil)
            let rates = jsonData["rates"].dictionary!
            for rate in rates{
                let location = rate.key
                let exchangeRate = self.roundTo(places: 2, value: rate.value.doubleValue)
                
                let cellRate = ExchangeRate(location: location, exchangeRate: String(exchangeRate))
                result.append(cellRate)
            }
            
            semaphore.signal()
            
        }.resume()
        
        _ = semaphore.wait(timeout: .distantFuture)

        return result
    }
    
    private func roundTo(places : Int, value : Double) -> Double {
        let divisor = pow(10.0, Double(places))
        return (divisor * value).rounded() / divisor;
    }
    
}

