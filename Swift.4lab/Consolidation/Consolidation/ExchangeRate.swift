//
//  ExchangeRate.swift
//  TabBarApplication
//
//  Created by Admin on 20.02.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import Foundation

class ExchangeRate{
    var location : String = ""
    var exchangeRate: String = ""
    
    
    init(location : String, exchangeRate : String){
        
        self.location = location
        self.exchangeRate = exchangeRate
    }
}
